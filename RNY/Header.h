#include <math.h>
#include <iostream>
#include <conio.h>
#pragma once
using namespace std;
#define YRAVNENIE(x) 1.2*pow(x,2)-sin(10*x)
#define PROIZVODN(x) 2.4*x-10*cos(10*x)
#define NAMBER_INTERVALS 4
#define EPS 0.0001
class MAIN_PART
{
protected:
	MAIN_PART()
	{
	};
	const double start_interval[NAMBER_INTERVALS][2] = { { -0.1,0.25 }, {0.25,0.5},{0.6,0.7},{0.8,1} };
	void Out(char * name, int * iteraci, double * answer)
	{
		cout << "================================================================\n" << name << endl;
		for (int i = 0; i < NAMBER_INTERVALS; i++)
		{

			cout << "iterasi: " << iteraci[i] << endl << "Answer: " << answer[i] << endl<<"----------------------------------\n";
		}
		cout << "================================================================\n";
	}
};
class  BIECTION:MAIN_PART
{
public:
	void MakeMagic()
	{
		double * answer = new double[NAMBER_INTERVALS];
		double ab[2];
		double c;
		int * iteraci = new int[NAMBER_INTERVALS];
		for (int i = 0; i < NAMBER_INTERVALS; i++)
		{
			iteraci [i] = 0;
			ab[0] = start_interval[i][0];
			ab[1] = start_interval[i][1];
			double hh = abs(ab[1] - ab[0]);
			while (abs(ab[1] - ab[0]) > EPS)
			{
				iteraci[i]++;
				c = (ab[0] + ab[1]) / 2;
				double ty = YRAVNENIE(c);
				double ty2 = YRAVNENIE(ab[0]);
				if ((ty*ty2) < 0)
				{
					ab[1] = c;
				}
				else
				{
					ab[0] = c;
				}
			};
			answer[i] = (ab[0] + ab[1]) / 2;
		};
		Out("Biection met", iteraci, answer);
	}
	BIECTION()
	{
		MakeMagic();
	}
};
class NUTON :MAIN_PART
{
public:
	NUTON ()
	{
		double * answer = new double[NAMBER_INTERVALS];
		double x;
		double tmp_x;
		int * iteraci = new int[NAMBER_INTERVALS];
		for (int i = 0; i < NAMBER_INTERVALS; i++)
		{
			x = start_interval[i][0];
			iteraci[i] = 0;
			double x_tmp;
			for (bool ex = 1;ex;)
			{
				iteraci[i]++;
				double ty = YRAVNENIE(x);
				double ty2 = PROIZVODN(x);
				x_tmp = x -  ty/ty2;
				if (abs(x_tmp - x)<=EPS)
					ex = 0;
				x = x_tmp;
			}
			answer[i] = x;
		};
		Out("Nuton method",iteraci,answer);
	}
};