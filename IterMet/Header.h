#include <iostream>
#include <vector>
#include <conio.h>
#include <math.h>
#include <time.h>
#define K 0.1
#define E 0.0001
#pragma once
using namespace std;
class OsnMet
{
protected:
	const double D[16] = {6.22,1.42,-1.72,1.91,1.42,5.33,1.11,-1.82,-1.72,1.11,5.24,1.42,1.91,-1.82,1.42,6.55};
	const double C[16] = {1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1};
	const double b[4] = {7.53,6.06,8.05,8.06};
	double * A;
	void ChekResult(const double * x, int size = 16, int size_str = 4)
	{
		double * out = new double[size_str];
		double summ = 0;
		for (int i = 0; i < size_str; i++)
		{
			summ = 0;
			for (int j = 0; j < size_str; j++)
			{
				summ += A[i*size_str + j] * x[j];
			}
			out[i] = summ - b[i];
		}
		OutMatrix(out, 4, 1);
	}
	void OutMatrix(const double * mass, int size, int size_str)
	{
		for (int i = 0; i < size; i++)
		{
			if (!(i%size_str))
				cout << endl;
			cout << mass[i] << "\t\t";
		}
		cout << endl << endl;
	}
	double * SymmMatrix(const double * first, const  double * second, int size)
	{
		double * out = new double[size];
		for (int i = 0; i < size; i++)
			out[i] = first[i] + second[i];
		return out;
	}
	double * ProizvedMatrixOnK(const double * first, double k, int size)
	{
		double * out = new double[size];
		for (int i = 0; i < size; i++)
			out[i] = first[i] * k;
		return out;
	}
	OsnMet()
	{
		cout << "Input data\n===============================================================\n";
		cout << "k " << K << endl << "eps " << E << endl;
			cout << "D" << endl;
			OutMatrix(D, 16, 4);
			cout << "C" << endl;
			OutMatrix(C, 16, 4);
			cout << "b" << endl;
			OutMatrix(b, 4, 1);
			A = ProizvedMatrixOnK(C, K, 16);
			A = SymmMatrix(D, A, 16);
			cout << "A" << endl;
			OutMatrix(A, 16, 4);
			cout << "===============================================================\n===============================================================\n";
	}
};
class FirstMet : public OsnMet
{
protected:
	double * beta, *alfa, *answer = new double[4];
	FirstMet()
	{
		beta = new double[4];
		alfa = new double[16];
		for (short int i = 0; i < 4; i++)
			beta[i] = b[i] / A[i * 5];
		cout << "beta" << endl;
		OutMatrix(beta, 4, 1);
		for (short int i = 0; i < 16; i++)
		{
			if (i % 5 == 0)
				alfa[i] = 0;
			else
				alfa[i] = (-1.0)*A[i] / A[5 * (i / 4)];
		}
		cout << "alfa" << endl;
		OutMatrix(alfa, 16, 4);
		double * tmp = new double [4];
		for (int i = 0; i < 4; i++)
			answer[i] = beta[i];
		bool end = 0;
		int schet = 0;
		do
		{
			for (int i = 0; i < 4; i++)
				tmp[i] = answer[i];
			double max = 0;
			for (int i = 0; i < 4; i++)
			{
				double tmp_sum = 0;
				for (int j = 0; j<4; j++)
					tmp_sum += tmp[j] * alfa[i * 4 + j];
				answer[i] = tmp_sum + beta[i];
				if (abs(answer[i] - tmp[i])>max)
					max = abs(answer[i] - tmp[i]);
			}
			if (max < E)
				end = 1;
			schet++;
		} while (!end);
		cout << "Simple iter\n===============================================================\nAnswer\n";
		OutMatrix(answer, 4, 1);
		cout << "Check Answer\n";
		ChekResult(answer);
		cout << "Iteraci  " << schet<<endl<<"==============================================================="<<endl;

	}
};
class SecondMet : public FirstMet
{
public :
	SecondMet()
	{
		answer = new double[4];
		double * tmp = new double[4];
		for (int i = 0; i < 4; i++)
			answer[i] = beta[i];
		bool end = 0;
		int schet = 0;
		do
		{
			for (int i = 0; i < 4; i++)
				tmp[i] = answer[i];
			double max = 0;
			for (int i = 0; i < 4; i++)
			{
				double tmp_sum  = 0;
				for (int j = 0; j<4; j++)
				{
					if (j<i)
					{
						tmp_sum += answer[j] * alfa[i * 4+j];
					}
					else
					{
						tmp_sum += tmp[j] * alfa[i * 4 + j];
					}
				}
				answer[i] =tmp_sum + beta[i];
				if (abs(answer[i] - tmp[i])>max)
					max = abs(answer[i] - tmp[i]);
			}
			if (max < E)
				end = 1;
			schet++;
		} while (!end);
		cout << "Zelder iter\n===============================================================\nAnswer\n";
		OutMatrix(answer, 4, 1);
		cout << "Check Answer\n";
		ChekResult(answer);
		cout << "Iteraci  " << schet << endl << "===============================================================" << endl;
	}
};