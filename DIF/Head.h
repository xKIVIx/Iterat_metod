#include <iostream>
#include <conio.h>
#pragma once
using namespace std;
class MAIN
{
private:
	//������� ������
	const double a = 1.0;
	const double b = 1.0;
	const double c = 1.0;
	const double h = 0.1;
	const double x0 = 0.0;
	const double xn = 1.0;


	double ** output_matrix;
	void Out(int size)
	{
		cout << "First met\tSecond met\tStandart\n";
		for (int i = 0; i <= size; i++)
		{
			cout << output_matrix[0][i] << "     \t"<< output_matrix[1][i]<<"     \t"<< output_matrix[2][i];
			cout << endl;
		}
		cout << "Pogreshnost\n";
		for (int i = 0; i <= size; i++)
		{
			cout << abs(output_matrix[2][i] - output_matrix[0][i]) << "     \t" << abs(output_matrix[2][i] - output_matrix[1][i]) << "     \t";
			cout << endl;
		}

	}
	double F(double x, double y)
	{
		return (a - b*y) / (c - x);
	}
	void FirstMet(int size)
	{
		output_matrix[0][0] = output_matrix[2][0];
		for (int i = 1; i <= size; i++)
		{
			output_matrix[0][i] = output_matrix[0][i - 1] + h*F(x0+i*h, output_matrix[0][i - 1]);
		}
	}
	double k2 (double x, double y)
	{
		return 2*F(x + h / 2, y + h*F(x, y) / 2);
	}
	double k3(double x, double y)
	{
		return 2*F(x + h / 2, y + h*k2(x, y) / 2);
	}
	double k4(double x, double y)
	{
		return F(x + h, y + h*k3(x, y));
	}
	void SecondMet(int size)
	{
		output_matrix[1][0] = output_matrix[2][0];
		for (int i = 1; i <= size; i++)
		{
			output_matrix[1][i] = output_matrix[1][i - 1] + h*(F(x0 + i*h, output_matrix[0][i - 1]) + k2(x0 + i*h, output_matrix[0][i - 1]) + k3(x0 + i*h, output_matrix[0][i - 1]) + k4(x0 + i*h, output_matrix[0][i - 1])) / 6;
		}
	}
public:
	MAIN()
	{
		int  size = abs(xn - x0)/h;
		output_matrix = new double *[3];
		for (int i = 0; i < 3; i++)
			output_matrix[i] = new double[size+1];
		for (int i = 0; i <= size; i++)
		{
			output_matrix[2][i] = a / b - pow(c - (x0 + i*h), b)*a / (pow(c, b)*b);
		}
		FirstMet(size);
		SecondMet(size);
		Out(size);
	}
};
