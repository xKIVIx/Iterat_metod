#include <iostream>
#include <vector>
#include <conio.h>
#include <math.h>
#define K 0.1
#pragma once
using namespace std;
void OutMatrix(const double * mass,int size,int size_str)
{
	for (int i = 0; i < size; i++)
	{
		if (!(i%size_str))
			cout << endl;
		cout << mass[i] << "\t\t";
	}
	cout << endl<<endl;
}
double * SymmMatrix(const double * first, const  double * second , int size)
{
	double * out = new double[size];
	for (int i = 0; i < size; i++)
		out [i] = first[i] + second[i];
	return out;
}
double * ProizvedMatrixOnK(const double * first,double k, int size)
{
	double * out = new double [size];
	for (int i = 0; i < size; i++)
		out[i] = first[i] * k;
	return out;
}
double * GetTMatrix(const double * in, int size,int size_str)
{
	double * out = new double[size];
	for (int i = 0; i < size; i++)
	{
		out[i] = 0;
	}
	double tmp = 0;
	for (int i = 0; i < size_str; i++)
	{
		tmp = 0;
		for (int k = 0; k < i; k++)
			tmp += out[k*size_str + i]* out[k*size_str + i];
		out[i*size_str + i] =  sqrt(in[i*size_str + i] - tmp);
		for (int j = i + 1; j < size_str; j++)
		{
			tmp = 0;
			for (int k = 0; k < i; k++)
				tmp += out[k*size_str + i] * out[k*size_str + j];
			out[i*size_str + j] = (in[i*size_str + j] - tmp) / out[i*size_str + i];
		}
	}
	return out;
}
double * GetObrMatrix(const double * in, int size, int size_str)
{
	double * out = new double [size];
	for (int i = 0; i < size_str; i++)
	{
		for (int j = 0; j < size_str; j++)
		{
			out[i*size_str + j] = in[j*size_str + i];
		}
	}
	return out;
}
double * GetX(const double * in, const double * b, int size, int size_str)
{
	double * out = new double[size_str];
	double tmp = 0;
	for (int i = size_str - 1; i >= 0; i--)
	{
		tmp = 0;
		for (int k = i +1; k < size_str; k++)
			tmp += in[i * size_str + k] * out[k];
		out[i] = (b[i] - tmp) / in[i*size_str + i];
	}
	cout << "x" << endl;
	OutMatrix(out, 4, 1);
	return out;
}
double * GetY(const double * in,const double * b, int size, int size_str)
{
	double * out = new double[size_str];
	double tmp = 0;
	for (int i = 0; i < size_str; i++)
	{
		tmp = 0;
		for (int k = 0; k < i; k++)
			tmp += in[k*size_str + i] * out[k];
		out[i] = (b[i] - tmp) / in[i*size_str + i];
	}
	cout << "y" << endl;
	OutMatrix(out, 4, 1);
	return GetX(in, out, 16, 4);
}
void ChekResult(const double * A ,const double * x,const double * b,int size,int size_str)
{
	double * out = new double[size_str];
	double summ = 0;
	for (int i = 0; i < size_str;i++)
	{ 
		summ = 0;
		for (int j = 0; j < size_str; j++)
		{
			summ += A[i*size_str + j] * x[j];
		}
		out[i] = summ - b[i];
	}
	OutMatrix(out,4,1);
}